<!-- data for home screen -->
<?php

$department = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

// session_start();

// if ($_SERVER["REQUEST_METHOD"] == "POST") {
//     $_SESSION = $_POST;
// }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="theme_home.css">
</head>

<body>
    <div class="wrapper">

        <div class="form-control">
            <form method="POST">

                <!-- selection department -->
                <div class="depart-label">
                    <label class="label-custom-1">Khoa</label>

                    <select class="custom" name="postDepartment" id="selector">
                        <!-- option empty -->
                        <option value=""></option>
                        <?php
                        foreach (array_keys($department) as $dep) {
                            echo '
                                <option value="' . $dep . '">' . $department[$dep] . '</option>
                            ';
                        }
                        ?>
                    </select>

                    <script>
                        var selectKhoa = document.getElementById("selector");
                        selectKhoa.value = "<?php echo $_POST['postDepartment'] ?>"
                    </script>

                </div>

                <!-- Key words -->
                <div class="keyword-label">
                    <label class="label-custom-1">Từ khóa</label>
                    <input class="input-keyword" name="postKeyword" type="text" id="keyword" value="<?php echo isset($_POST['postKeyword']) ? $_POST['postKeyword'] : ''  ?>">
                </div>

                <!-- search button -->
                <div class="btn-search-and-delete">
                    <button type="button" class="btn-custom-3" id="delete">Xóa</button>
                    <button class="btn-custom-1" id="search">Tìm kiếm</button>
                </div>

                <script>
                    var btnDel = document.getElementById("delete");
                    var selectKhoa = document.getElementById("selector");
                    var keyWord = document.getElementById("keyword");
                    btnDel.onclick = function() {
                        selectKhoa.value = "";
                        keyWord.value = "";
                    };
                </script>

            </form>
        </div>

        <div class="row-content">
            <label for="">Số sinh viên tìm thấy: </label> <span>XXX</span>
        </div>

        <form action="form.php">
            <div class="btn-add">
                <button type="submit" class="btn-custom-2">Thêm</button>
            </div>
        </form>

        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>